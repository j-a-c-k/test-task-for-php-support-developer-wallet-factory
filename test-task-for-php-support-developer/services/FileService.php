<?php

/**
 * Class FileService
 */
class FileService
{
    /**
     * Writing data in file
     * @param string $file
     * @param string $data
     * @return bool
     */
    public static function writeInFile(string $file, string $data): bool
    {
        return file_put_contents($file, $data) ? true : false;
    }

    /**Get data from file
     * @param string $file
     * @return bool|string
     */
    public static function getContentFromFile(string $file)
    {
        return file_get_contents($file) ?? false;
    }
}