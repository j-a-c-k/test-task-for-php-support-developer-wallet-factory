<?php

require_once ROOT.'/config/Registry.php';

/**
 * Class DataHashing
 */
class DataHashing
{
    /**
     * Hashing by sha512
     * @param string $data
     * @return string
     */
    public static function hashing(string $data): string
    {
        $hash = hash("sha512", $data);
        $salt = Registry::$salt;
        $hashWithSalt = $salt. $hash;

        $hashResult = md5($hashWithSalt);

        return $hashResult;
    }

    /**
     * Checking the sum-hash
     * @param string $data
     * @param string $decryptDataHash
     * @return bool
     */
    public static function hashValidation(string $data, string $decryptDataHash): bool
    {
        $verificationHash = self::hashing($data);
        if ($verificationHash === $decryptDataHash)
        {
            return true;
        }
        return false;
    }

}