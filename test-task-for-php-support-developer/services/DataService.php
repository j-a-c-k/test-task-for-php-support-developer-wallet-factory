<?php

// POINT 2

require_once ROOT.'/services/DataValidate.php';
require_once ROOT.'/services/DataHashing.php';
require_once ROOT.'/services/OpensslRSA.php';
require_once ROOT.'/config/Registry.php';
require_once ROOT.'/verification/DataVerification.php';

class DataService
{
    private $data;

    const EMPTY_STRING = "[]";

    /**
     * DataService constructor.
     * @param string $data from index.php
     */
    public function __construct(string $data)
    {
        /* @var $data JSON */
        $this->data = $data;
    }

    public function getData(): string
    {
        return $this->data;
    }

    /**
     * Return result from point 2.
     * It return validated data or error
     */
    public function execute()
    {
        $data = $this->getData();
        $dataArrayValidate = $this->validate($data);

        $dataValidated = $dataArrayValidate['dataValidated'];
        $errors = $dataArrayValidate['errors'];

        /** Check if has errors when data validated */
        if (!($errors === self::EMPTY_STRING)) {
            return $errors;
        }
        /* @var $dataValidation JSON */
        $dataHash = $this->hash($dataValidated);

        $filePublKey = Registry::$pathToKey.Registry::$fileNamePublKey;
        $publicKey = OpensslRSA::getKey($filePublKey);
        $encryptData = OpensslRSA::encryptDataByPublKey($dataHash, $publicKey);

        //Entry Point 3
        /** Data encrypted from db */
        $DataVerification = new DataVerification($dataValidated, $encryptData);
        $encryptDataFromDb = $DataVerification->execute();

        if ($encryptDataFromDb) {

            $filePrivKey = Registry::$pathToKey . Registry::$fileNamePrivKey;
            $privateKey = OpensslRSA::getKey($filePrivKey);

            /** Data decrypted from db */
            $decryptDataFromDb = OpensslRSA::decryptDataByPrivKey($encryptDataFromDb, $privateKey);

            return $decryptDataFromDb;
        }
        return false;
    }

    /**
     * Return array of errors and data validated
     * @param string $data json
     * @return array
     */
    private function validate(string $data): array
    {
        $dataArray = json_decode($data, true);
        $validation = new DataValidate($dataArray);

        $dataValidated = json_encode($validation->execute());
        $errors = json_encode($validation->getErrors());

        return array(
            'errors' => $errors,
            'dataValidated' => $dataValidated
        );
    }

    /**
     * @param string $data DataValidate
     * @return string
     */
    private function hash(string $data): string
    {
        $hash = DataHashing::hashing($data);

        return $hash;
    }
}