<?php

require_once ROOT.'/services/FileService.php';
require_once ROOT.'/config/Registry.php';

class OpensslRSA
{
    private $filePublKey;
    private $filePrivKey;

    public $config = array(
        "digest_alg" => "sha512",
        "private_key_bits" => 4096,
        "private_key_type" => OPENSSL_KEYTYPE_RSA,
    );


    public function __construct()
    {
        $this->filePublKey = Registry::$pathToKey.Registry::$fileNamePublKey;
        $this->filePrivKey = Registry::$pathToKey.Registry::$fileNamePrivKey;
    }

    public function execute(): void
    {
        $filePublKey = $this->filePublKey;
        $filePrivKey = $this->filePrivKey;

        $keys = $this->generateKeys();

        $dataPrivKey = $this->extractPrivateKey($keys);
        $dataPublKey = $this->extractPublicKey($keys);

        FileService::writeInFile($filePrivKey, $dataPrivKey);
        FileService::writeInFile($filePublKey, $dataPublKey);
    }

    /**
     * Creating the private and public key
     */
    public function generateKeys()
    {
        return openssl_pkey_new($this->config);
    }

    /**
     * Extracting the private key from $keys to $privateKey
     */
    public function extractPrivateKey($keys): string
    {
        openssl_pkey_export($keys, $privKey);

        return $privKey;
    }

    /**
     * Extracting the public key from $keys to $publicKey
     */
    public function extractPublicKey($keys): string
    {
        $pubKey = openssl_pkey_get_details($keys);

        return $pubKey["key"];
    }

    /**
     * Encrypting the data by using the public key and store the results in $encrypted
     * @param $data
     * @param $publicKey
     * @return string $encrypted
     */
    public static function encryptDataByPublKey(string $data, $publicKey): string
    {
        openssl_public_encrypt($data, $encrypted, $publicKey);

        return $encrypted;
    }

    /**
     * Decrypting the ecryptData by using the private key and store the results in $decrypted
     * @param $encryptdata
     * @param $privateKey
     * @return string $decrypted
     */
    public static function decryptDataByPrivKey(string $encryptdata, $privateKey): string
    {
        openssl_private_decrypt($encryptdata, $decrypted, $privateKey);

        return $decrypted;
    }

    public static function getKey(string $file): string
    {
        return FileService::getContentFromFile($file);
    }

}