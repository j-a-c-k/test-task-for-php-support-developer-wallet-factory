<?php

// POINT 3

require_once ROOT.'/config/Registry.php';
require_once ROOT.'/services/OpensslRSA.php';
require_once ROOT.'/services/DataHashing.php';
require_once ROOT.'/components/DataBase.php';
require_once ROOT.'/entity/User.php';
require_once ROOT.'/database/UserMapper.php';

/**
 * Class DataVerification
 */
class DataVerification
{
    private $encryptData;
    private $data;

    /**
     * DataVerification constructor.
     * @param string $data DataValidate
     * @param string $encryptData DataService
     */
    public function __construct(string $data, string $encryptData)
    {
        $this->encryptData = $encryptData;
        $this->data = $data;
    }

    /**
     * Return result from point 3
     */
    public function execute()
    {
        $encryptData = $this->getEncryptData();
        $data = $this->getData();

        $decryptDataHash = $this->decryptData($encryptData);

        $result = $this->isHashValid($data, $decryptDataHash);

        if ($result) {
            /** connection to db*/
            $db = DataBase::getInstance();
            $user = new User($data);
            $mapper = new UserMapper($db);

            /** User's data saving to db */
            $mapper->save($user);

            $id = $user->getId();
            /** Data get from db by id */
            $dataFromDb = $mapper->findById($id)->getUserData();

            $filePublKey = Registry::$pathToKey.Registry::$fileNamePublKey;
            $publicKey = OpensslRSA::getKey($filePublKey);
            $encryptDataFromDb = OpensslRSA::encryptDataByPublKey($dataFromDb, $publicKey);

            return $encryptDataFromDb;
        }
        return false;
    }

    public function getEncryptData(): string
    {
        return $this->encryptData;
    }

    public function getData(): string
    {
        return $this->data;
    }

    /**
     * @param string $encryptData from DataService
     * @return string
     */
    private function decryptData(string $encryptData): string
    {
        $filePrivKey = Registry::$pathToKey.Registry::$fileNamePrivKey;

        $privateKey =  OpensslRSA::getKey($filePrivKey);

        $decryptDataHash = OpensslRSA::decryptDataByPrivKey($encryptData, $privateKey);

        return $decryptDataHash;
    }

    /**
     * @param string $data from DataValidate
     * @param string $decryptDataHash
     * @return bool
     */
    private function isHashValid(string $data, string $decryptDataHash): bool
    {
        $resultValidation = DataHashing::hashValidation($data, $decryptDataHash);

        return $resultValidation;
    }

}