<?php

//POINT 1

define('ROOT', dirname(__FILE__));

require_once ROOT.'/services/DataService.php';
require_once ROOT.'/services/OpensslRSA.php';
require_once ROOT.'/services/FileService.php';
require_once ROOT.'/config/Registry.php';

error_reporting(E_ALL);


$fileContentPubKey = FileService::getContentFromFile(Registry::$pathToKey.Registry::$fileNamePublKey);
$fileContentPriKey = FileService::getContentFromFile(Registry::$pathToKey.Registry::$fileNamePrivKey);

/** Checking if keys exist, if not then create new keys */
if (!($fileContentPubKey && $fileContentPriKey)) {
    (new OpensslRSA())->execute();
}

$request = $_POST['json'];

// Entry Point 2
$dataService = (new DataService($request))->execute();

echo $dataService;